#ifndef RUBBERDUK_RUBBERDUK_HPP
#define RUBBERDUK_RUBBERDUK_HPP

#include<string>
#include<functional>
#include<vector>
#include<iostream>
#include<cstring>
#include<unordered_map>
#include<type_traits>

#include<duktape/duktape.h>

namespace rubberduk
{
    using duk_function_t = std::function<duk_ret_t(duk_context*)>;

    namespace detail
    {
        template<typename T>
        inline T duk_get_arg(duk_context* ctx, int i);

        template<>
        inline std::string duk_get_arg<std::string>(duk_context* ctx, int i)
        {
            std::string ret(duk_require_string(ctx, i));
            return ret;
        }

        template<>
        inline int duk_get_arg<int>(duk_context* ctx, int i)
        {
            int ret = duk_require_int(ctx, i);
            return ret;
        }

        template<>
        inline unsigned int duk_get_arg<unsigned int>(duk_context* ctx, int i)
        {
            unsigned int ret = duk_require_int(ctx, i);
            return ret;
        }

        template<>
        inline float duk_get_arg<float>(duk_context* ctx, int i)
        {
            float ret = (float)duk_require_number(ctx, i);
            return ret;
        }

        template<>
        inline double duk_get_arg<double>(duk_context* ctx, int i)
        {
            double ret = duk_require_number(ctx, i);
            return ret;
        }

        template<typename T_Return, typename ... T_Params, size_t ... T_Is>
        inline T_Return duk_get_args_impl(duk_context* ctx, std::function<T_Return(T_Params ...)>& function_item, std::index_sequence<T_Is ...>)
        {
            using tuple_type = std::tuple<T_Params ...>;
            T_Return ret;
            ret = function_item(duk_get_arg<std::tuple_element_t<T_Is, tuple_type>>(ctx, T_Is) ...);
            return ret;
        }

        template<typename T, typename ... T_Params, size_t ... T_Is>
        inline T* duk_get_args_constr_impl(duk_context *ctx, std::function<T*(T_Params ...)>& constructor_item, std::index_sequence<T_Is ...>)
        {
            using tuple_type = std::tuple<T_Params ...>;
            T* ret;
            ret = constructor_item(duk_get_arg<std::tuple_element_t<T_Is, tuple_type>>(ctx, T_Is) ...);
            return ret;
        }

        template<typename T_Return, typename ... T_Params>
        inline T_Return duk_get_args(duk_context* context, std::function<T_Return(T_Params ...)>& function_item)
        {
            T_Return ret;
            ret = duk_get_args_impl<T_Return, T_Params ...>(context, function_item, std::index_sequence_for<T_Params ...>());
            return ret;
        }

        template<typename T, typename ... T_Params>
        inline T* duk_get_args_constr(duk_context* context, std::function<T*(T_Params ...)>& constructor_item)
        {
            T* ret;
            ret = duk_get_args_constr_impl<T, T_Params ...>(context, constructor_item, std::index_sequence_for<T_Params ...>());
            return ret;
        }

        template<typename T, typename ... T_Params>
        inline T* duk_constr_proxy(T_Params ... argument_items)
        {
            return new T(argument_items ...);
        }

        template<typename T>
        inline void duk_return(duk_context* ctx, T item);

        template<>
        inline void duk_return<std::string>(duk_context* ctx, std::string item)
        {
            duk_push_string(ctx, item.c_str());
        }

        template<>
        inline void duk_return<int>(duk_context* ctx, int item)
        {
            duk_push_int(ctx, item);
        }
        template<>
        inline void duk_return<unsigned int>(duk_context* ctx, unsigned int item)
        {
            duk_push_uint(ctx, item);
        }
        template<>
        inline void duk_return<float>(duk_context* ctx, float item)
        {
            duk_push_number(ctx, (double)item);
        }
        template<>
        inline void duk_return<double>(duk_context* ctx, double item)
        {
            duk_push_number(ctx, item);
        }
    }

    class DukFunction
    {
    public:
        DukFunction(std::string name_item)
        {
            m_Name = name_item;
        }

        template<typename T_Return, typename ... T_Params>
        void bindFunction(T_Return(*function_item)(T_Params ...))
        {
            std::function<T_Return(T_Params ...)> proxy_func(function_item);
            duk_function_t func = [proxy_func] (duk_context* ctx) mutable -> duk_ret_t {
                const int n_Args = sizeof...(T_Params);
                if(duk_get_top(ctx)==n_Args)
                {
                    if(std::is_same<T_Return, void>::value)
                    {
                        detail::duk_get_args<T_Return, T_Params ...>(ctx, proxy_func);
                        return 0;
                    }
                    else
                    {
                        T_Return ret = detail::duk_get_args(ctx, proxy_func);
                        detail::duk_return(ctx, ret);
                        return 1;
                    }
                }
                else
                {
                    duk_error(ctx, DUK_ERR_ERROR, "Invalid argument(s).");
                    return 0;
                }
            };
            m_Function = func;
        }

        template<typename T_Return, typename ... T_Params>
        void bindFunction(std::function<T_Return(T_Params ...)> function_item)
        {
            duk_function_t func = [function_item] (duk_context* ctx) mutable -> duk_ret_t {
                const int n_Args = sizeof...(T_Params);
                if(duk_get_top(ctx)==n_Args)
                {
                    if(std::is_same<T_Return, void>::value)
                    {
                        detail::duk_get_args<T_Return, T_Params ...>(ctx, function_item);
                        return 0;
                    }
                    else
                    {
                        T_Return ret = detail::duk_get_args(ctx, function_item);
                        detail::duk_return(ctx, ret);
                        return 1;
                    }
                }
                else
                {
                    duk_error(ctx, DUK_ERR_ERROR, "Invalid argument(s).");
                    return 0;
                }
            };
            m_Function = func;
        }

        duk_function_t getFunction()
        {
            return m_Function;
        }
        std::string getName()
        {
            return m_Name;
        }
    private:
        duk_function_t m_Function;
        std::string m_Name;
    };

    class DukClass
    {
    public:
        DukClass(std::string name);
        ~DukClass();
        template<typename T, typename ... T_Params>
        void setConstructor()
        {
            std::function<T*(T_Params ...)> constructor_item(detail::duk_constr_proxy<T, T_Params ...>);
            std::function<bool(duk_context*)> constructor = [constructor_item] (duk_context* ctx) mutable {
               constexpr const unsigned int n_Args = sizeof...(T_Params);
               if(duk_get_top(ctx)==n_Args)
               {
                   T* __object_pointer;
                   __object_pointer = detail::duk_get_args_constr(ctx, constructor_item);
                   duk_pop_n(ctx, n_Args);
                   duk_push_this(ctx);
                   duk_push_pointer(ctx, (void*)__object_pointer);
                   duk_put_prop_string(ctx, 0, "\xff""\xff""__object_pointer");
                   return true;
               }
               else
               {
                   duk_error(ctx, DUK_ERR_ERROR, "Wrong number of arguments.");
                   return false;
               }
            };
        }
        template<typename T, typename T_Return, typename ... T_Params>
        void addMethod(T_Return(T::*method_item)(T_Params ...), std::string name)
        {
            duk_function_t method = [method_item] (duk_context* ctx) -> duk_ret_t {
                const int n_Args = sizeof...(T_Params);
                if(duk_get_top(ctx)==n_Args)
                {
                    if(std::is_same<T_Return, void>::value)
                    {

                        return 0;
                    }
                    else
                    {

                        return 1;
                    }
                }
                else
                {
                    duk_error(ctx, DUK_ERR_ERROR, "Invalid argument(s).");
                    return 0;
                }
            };
        }

        std::string getName()
        {
            return m_Name;
        }
        std::function<void(duk_context*)> getConstructor()
        {
            return m_Constructor;
        }
        std::unordered_map<std::string, duk_function_t>& getMethods()
        {
            return m_Methods;
        }
    private:
        std::string m_Name;
        std::function<bool(duk_context*)> m_Constructor;
        std::unordered_map<std::string, duk_function_t> m_Methods;
    };

    class DukModule
    {
    public:
        DukModule(std::string modulename_item);
        ~DukModule();

        void add(DukFunction& dukfunction_item);
        void add(DukClass& dukclass_item);
    private:
        std::vector<DukFunction> m_Functions;
        std::vector<DukClass> m_Classes;
        std::unordered_map<std::string, std::unordered_map<std::string, void*>> m_Objects;
    };

    class DukContext
    {
    public:
        DukContext(std::ostream* output_item = &std::cout);
        DukContext(DukContext& dukcontext_item, std::ostream* output_item = &std::cout);
        ~DukContext();

        /** \brief Evaluate an ECMAScript statement.
         *
         * \param code_item Code statement as string.
         */
        void eval(std::string code_item);
        /** \brief Evaluate an ECMAScript file.
         *
         * \param filename_item Path to file.
         */
        void evalFile(std::string filename_item);

        /** \brief Compiles an ECMAScript statement to a bytecode file.
         *
         * \param outfilename_item Path to output file
         * \param code_item Code statement to compile
         *
         */
        void compile(std::string outfilename_item, std::string code_item);
        /** \brief Compiles an ECMAScript file to a bytecode file.
         *
         * \param outfilename_item Path to output file
         * \param infilename_item Path to input file
         *
         */
        void compileFile(std::string outfilename_item, std::string infilename_item);
        /** \brief Load a precompiled bytecode file.
         *
         * \param headerfilename_item Path to bytecode header file
         *
         */
        void load(std::string headerfilename_item);

        void add(DukFunction& dukfunction_item);
        void add(DukClass& dukclass_item);
        void add(DukModule& dukmodule_item);

        static duk_ret_t handleFunction(duk_context* ctx);
        static duk_ret_t handleMethod(duk_context* ctx);
        static duk_ret_t handleConstructor(duk_context* ctx);
    private:
        duk_context* m_ContextHandle;
        std::ostream* m_LogOutput;
        std::unordered_map<std::string, duk_function_t*> m_RegisteredFunctions;

    };
}
#endif // RUBBERDUK_RUBBERDUK_HPP
